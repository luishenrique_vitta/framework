const merge = require('webpack-merge');
const common = require('./webpack.common.js');

module.exports = merge(common, {
    devtool: 'inline-source-map',
    devServer: {
        contentBase: './web',
        https: true,
        port: 9001,
        inline: true,
        watchOptions: {
            aggregateTimeout: 300,
            poll: 1000
        },
        hot: true,
        open: false,
        historyApiFallback: true
    },
    mode: "development"
});
