const path = require("path");
const _ = require("lodash");
const webpack = require("webpack");
const fs = require("fs");
const Dotenv = require('dotenv-webpack');
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const winston = require('winston');
const logger = new(winston.Logger)({
    transports: [
        new(winston.transports.Console)(),
    ]
});

const getPath = relativePath => path.resolve(__dirname, relativePath);

// Each app should be registered in here
const entries = {
    Auth: [getPath("src/js/apps/Auth/bootstrap.js")],
};


// @todo improve this!!!
const environment = process.env.NODE_ENV;

const appConfig = (function () {
    const defaultEnvFile = ".env.js";
    const exampleEnvFile = getPath(".env.example.js");
    let envFile = getPath(`.env.${environment}.js`);
    if (!fs.existsSync(envFile)) {
        logger.info(`Enviroment file not found for ${environment}`);
        envFile = getPath(defaultEnvFile);
    }

    if (!fs.existsSync(envFile)) {
        logger.warn(`Enviroment file not found for ${environment}`);

        if (!fs.existsSync(exampleEnvFile)) {
            throw Error("No environment file found neither example file.");
        }

        envFile = exampleEnvFile;
    }

    const envConfigs = require(envFile);
    return Object.keys(envConfigs).reduce((configs, config) => {
        configs[config] = process.env[config] || envConfigs[config];
        return configs;
    }, {
        environment: environment
    });
}());


// Expect that via enviroment variable `entries` you can specify which app should  be built
// Example: env entries=app1,app2 yarn dev => Will buid app1 and app2
const entriesToLoad = process.env.entries ? process.env.entries.split(",") : Object.keys(entries);
console.log(entriesToLoad, _.pick(entries, entriesToLoad));

module.exports = {
    entry: _.pick(entries, entriesToLoad),
    output: {
        path: getPath("web/js"),
        filename: "[name].js",
        library: "App",
        libraryTarget: "umd",
        libraryExport: "default",
        publicPath: "js"
    },
    resolve: {
        alias: {
            '@': getPath("src/js"),
            app: getPath("src/js/app.js"),
            core: getPath("src/js/core"),
            css: getPath("src/css"),
            shared: getPath("src/js/shared"),
        },
        extensions: ['.js', '.vue', '.styl'],
    },
    module: {
        rules: [{
            test: /\.css$/,
            loader: 'vue-style-loader!css-loader'
        }, {
            test: /\.(woff2?|eot|ttf|otf|svg)(\?.*)?$/,
            exclude: getPath("./src"),
            loader: 'url-loader?limit=100000',
            options: {
                name: path.posix.join('/assets', 'fonts/[name].[ext]'),
            },
        }, {
            test: /\.vue$/,
            exclude: /node_modules/,
            loader: 'vue-loader',
            options: {
                presets: ['es2015', 'stage-2'],
                loaders: {
                    stylus: 'vue-style-loader!css-loader!postcss-loader!stylus-loader',
                },
            },
        }, {
            test: /\.js$/,
            exclude: /node_modules/,
            loader: 'babel-loader',
            options: {
                presets: ['es2015', 'stage-2'],
            },
        }, {
            test: /\.(png|woff|woff2|jpg|gif|svg)(\?v=.*)?$/,
            exclude: /node_modules/,
            loader: 'url-loader?limit=100000',
            options: {
                name: '/assets/images/[name].[ext]',
            },
        }, ],
    },
    plugins: [
        new Dotenv({
            path: './.env',
        }),
        new VueLoaderPlugin(),
        new webpack.DefinePlugin({
            APP_CONFIG: JSON.stringify(appConfig)
        })
    ]
};
