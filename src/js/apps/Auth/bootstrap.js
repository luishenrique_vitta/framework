import App from "app";

import AuthService from "./services/Auth";
App.ServicesContainer.setInstance("Auth", AuthService);


import LoginForm from "./components/LoginForm.vue";

import Login from "./pages/Login.vue";
import AnotherPage from "./pages/AnotherPage.vue";
import NotFound from "shared/pages/NotFound";

App.Auth = {
    components: {LoginForm},
    pages: {Login, AnotherPage, NotFound},
};


require("./routes");

// main sass file
// require("css/main.scss");

export default App;
