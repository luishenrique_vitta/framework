import App from "app";
import Router from "core/RouterVue";
import Page from "page";
import Layout from "shared/layouts/Default";

const routerInstance = Router(document.querySelector("#react-root"), Page);

App.ServicesContainer.setInstance("ROUTER", routerInstance);

routerInstance.addMiddleware("*", (ctx, next) => {
    console.log("Router", ctx);
    next();
})

const Pages = App.Auth.pages;
routerInstance.setDefaultLayout(Layout);
routerInstance.setDefaultTitle("VITTA");
// debugger
routerInstance.addRoute("/", Pages.Login);
routerInstance.addRoute("/login", Pages.Login);
routerInstance.addRoute("/another", Pages.AnotherPage);

/******************************************************************************/
/* Not found ******************************************************************/
/******************************************************************************/
routerInstance.addRoute("*", {
    component: Pages.NotFound,
    title: "Ops!"
});


routerInstance.start();
