let currentUser = null;

export default {
    attempt(credentials) {
        const { username, password } = credentials;
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                if (username === "admin" && password === "admin") {
                    currentUser = {username, name: "Administrator"};
                    return resolve(currentUser);
                }

                reject({errorMessage: "Wrong credentials."});
            }, 1000);
        })
    },

    currentUser() {
        return currentUser;
    },

    logout() {
        return new Promise(resolve => {
            currentUser = null;
            resolve(true);
        });
    }
}
