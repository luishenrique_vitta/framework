import App from "app";

import NotFoundPage from "./pages/NotFound";
import DefaultLayout from "./layouts/Default";

App.shared = {
    layouts: {
        Default: DefaultLayout,
    },
    components: {

    },
    pages: {
        NotFound: NotFoundPage
    }
};
