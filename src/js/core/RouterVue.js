import App from "app";
import Vue from "vue";

/**
 *
 * @param {*} DOMNode
 * @param {*} Page
 * @name RouterEngine
 */
const RouterEngine = function (DOMNode, Page) {
    let defaultLayout = null;
    let defaultTitle = "App";

    const routesMap = {};
    const resolvers = {};

    const vueApp = new Vue({
        el: DOMNode,
        data: {
            Layout: null,
            Component: null,
        },
        render (createElement) {
            if (this.Layout) {
                return createElement(this.Layout, {}, [createElement(this.Component)]);
            }

            return createElement(this.Component);
        }
    });

    const createRoute = function (routeDefinition) {
        let route = {
            name: null,
            middleware: null,
            handler: null,
            component: null,
            layout: null,
            title: ""
        };

        // Checks if is an component
        if (typeof routeDefinition === "object" && routeDefinition !== null && "render" in routeDefinition === true) {
            routeDefinition = {
                component: routeDefinition,
                layout: defaultLayout
            };
        }

        // if (typeof routeDefinition === "function") {
        //     routeDefinition = {
        //         component: routeDefinition,
        //         layout: defaultLayout
        //     };
        // }

        route = Object.assign(route, routeDefinition);
        if (!route.title || !route.title.length) {
            route.title = defaultTitle;
        } else {
            route.title = `${route.title} | ${defaultTitle}`;
        }
        return route;
    };

    const renderPage = function (Page, context) {
        // vueApp.ViewComponent = Page;

        vueApp.Component = Page;
        vueApp.Layout = null;

        // RDOM.render(
        //     <Page { ...context.params } queryString={ context.query || {} } context={ context }/>,
        //     DOMNode
        // );
    };

    const renderPageWithLayout = function (Layout, Page, context) {
        // return renderPage(Page, context);

        vueApp.Component = Page;
        vueApp.Layout = Layout;

        // RDOM.render(
        //     <Layout>
        //         <Page { ...context.params } queryString={ context.query || {} } context={ context }/>
        //     </Layout>,
        //     DOMNode
        // );
    };

    const addRouteHandler = function (path, routeDefinition) {
        let layout = routeDefinition.layout || defaultLayout;
        let useHandler = routeDefinition.handler;
        if (layout && routeDefinition.component) {
            useHandler = renderPageWithLayout.bind(null, layout, routeDefinition.component);
        } else if (routeDefinition.component) {
            useHandler = renderPage.bind(null, routeDefinition.component);
        }

        if (typeof useHandler !== "function") {
            throw new Error(`Handler for "${path}" is not a function`);
        }

        Page(path, function (context) {
            let args = Object.keys(context.params);
            let resolver = args.reduce(function (resolver, argName) {
                if (!resolver && resolvers.hasOwnProperty(argName)) {
                    resolver = {
                        fn: resolvers[argName],
                        name: argName,
                        value: context.params[argName]
                    };
                }
                return resolver;
            }, null);

            if (resolver) {
                resolver.fn(resolver.value, context, () => {
                    if (routeDefinition.title.length) {
                        document.title = routeDefinition.title;
                    }
                    useHandler(context);
                });
            } else {
                if (routeDefinition.title.length) {
                    document.title = routeDefinition.title;
                }
                useHandler(context);
            }
        });
    };


    return {
        setDefaultLayout: (layout) => {
            defaultLayout = layout;
        },

        setDefaultTitle: (title) => {
            defaultTitle = title;
        },

        addResolver: (param, fn) => {
            resolvers[param] = fn;
        },

        addRoutes: (routes) => {
            Object.keys(routes).map(path => {
                routesMap[path] = createRoute(routes[path]);
                addRouteHandler(path, routesMap[path]);
            });
        },

        addRoute: (path, config) => {
            routesMap[path] = createRoute(config);
            addRouteHandler(path, routesMap[path]);
        },

        addMiddleware(path, fn) {
            Page(path, fn);
        },

        setRoute: function (route) {
            Page.redirect(route);
        },

        start: function () {
            Page.start({
                click: true,
                hashbang: false
            });
        },

        current() {
            return Page.current;
        }
    }
};

export default RouterEngine;
