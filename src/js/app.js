import Config from "./config";
import ServicesContainer from "core/ServicesContainer";
import EventManager from "sbee";

const App = global.App || {};

if ("Config" in App === false) {
    App.Config = Config;
    App.config = (name, defaultValue) => App.Config.get(name, defaultValue);
}

if ("ServicesContainer" in App === false) {
    App.ServicesContainer = new ServicesContainer();
    App.Service = name => App.ServicesContainer.get(name);
}

if ("EventManager" in App === false) {
    App.EventManager = new EventManager({ttl: 60});
    App.ServicesContainer.register("EventManager", EventManager);
}

export default App;
