
# Vitta Framework

This is a _webapp_ structure proposal that can be used by all Vitta projects.

It's based on  [Star Framework](http://github.com/beeblebrox3/star/).

## Vocabulary
**Project / Product / Application**
It's a full application, with back and front ends. It can be a Vitta Project, internal application or tool.

**App**
Small part of a Project. A self contained bundle to be shipped to the clients. An App can interact with  other Apps in the same Project, providing UI components, services, constants etc.

## The problem
Today we use [Vuejs](https://vuejs.org/), but we don't take advantage of the most important benefit: _reusability_. Each product uses a different  structure and even in the same project, a lot of stuff is unnecessary repeated in every module. Also, we can't reuse new code, made with Vue, on legacy code.

## Proposals
- Usage of _namespaces_, allowing new code to be used by legacy code;
- Centralized component to provide services for all Apps;
- Multiple bundles for small file sizes that can cooperate with each other.

## Demo
Clone this _repo_ and run:

```bash
# install dependencies
yarn

# run development server
yarn dev
```

## About the structure
When you use this structure on a project, the goal is that you can have multiple Apps. Each App will be a bundle with all pages, components and services that it needs to work.
On small or internal projects it isn't necessary, of course. But you need to have at least one.

See below an example for the Auth App:

```js
const App = {
    Auth: {
        components: {
            LoginForm,
            ResetPasswordForm,
        },
        pages: {
            Login
        },
        constants: {
            USER_TYPE: {
                ADMIN: "admin"
            }
        }
    }
}
```

The idea is that the App namespace will be available globally on the page, so each App can register itself on it.
If you need to render some Auth component in the legacy code, you need to do this:

```js
// oldcode.js
new Vue({
    el: "#app",
    render (createElement) {
      return createElement(App.Auth.components.LoginForm)
    }
});
```

### Directory structure

> The numbers on some lines are just references, used by the list below the figure.

```ruby
.
├── src
│   ├── img
│   └── js
│       ├── app.js                  /* [1] */
│       ├── apps                    /* [2] */
│       │   └── Auth                /* [3] */
│       │       ├── bootstrap.js    /* [4] */
│       │       ├── components      /* [5] */
│       │       │   └── LoginForm.js
│       │       ├── constants
│       │       │   └── USERTYPE.js
│       │       ├── pages           /* [6] */
│       │       │   └── Login.js
│       │       ├── routes.js       /* [7] */
│       │       └── services        /* [8] */
│       │           └── Auth.js
│       ├── config.js               /* [9] */
│       ├── core                    /* [10] */
│       │   ├── Config.js
│       │   ├── Router.js
│       │   └── ServicesContainer.js
│       └── shared                  /* [11] */
│           ├── components
│           ├── index.js             /* [12] */
│           ├── layouts              /* [13] */
│           │   └── Default.js
│           └── pages                /* [14] */
│               └── NotFound.js
├── web                              /* [15] */
│   ├── css
│   ├── img
│   ├── index.html
│   └── js
├── webpack.common.js
├── webpack.dev.js
└── yarn.lock

```
Let's understand each file/directory intentions:

#### `src/js/app.js` [1]
This file creates the App namespace and adds the core modules of the framework to this object - those modules are described on Core Modules section.
All Apps must  import this file. It has an alias to make it easy to use:

```js
// src/js/apps/MyApp/bootstrap.js
import App from "app";
```
If you need to add global objects to the Project, thats the place to do it.

#### `src/js/apps` [2]
This directory stores the applications's Apps. The concept of App should be accorded  by the team working on the project. Have in mind that each App will be a separated bundle. See the vocabulary.

#### `src/js/apps/Auth` [3]
In this example, the `Auth` directory is an App. All code related to the Auth App should live in this repository.

#### `src/js/apps/Auth/bootstrap.js` [4]
The `bootstrap.js` file is the _entrypoint_ of the App. It should create it's namespace on the global App, register it's services, components, pages, constants, routes etc.

Example:
```js
// src/js/Apps/Auth/bootstrap.js

// import the App namespace object
import App from "app";

// Register the App on the namespace
App.Auth = {
  components: {
    MyComponent: require("./components/MyComponent.vue")
  },
};

// Require the routes file if it has one
require("./routes.js")
```

#### `src/js/apps/Auth/components` [5]
This directory should contain you App components, like forms, tables, lists etc. Those components may be used by other Apps if necessary.

#### `src/js/Auth/pages` [6]
Pages components normally aren't reusable by other Apps. A page uses the App components to let users interact with the application, interact with  the services and external resources.

#### `src/js/Auth/routes.js` [7]
In this file you should register the routes of the App.

#### `src/js/Auth/services` [8]
Probably your App interacts with some API or browser resource, like Cookies, Local Storage etc. Your components, ideally, don't talk with those interfaces. Instead, they talk with a service. This service is shared across the entire project, so you can use those services on another App and in the legacy code. The services should be registered on the Services Container, that will be covered by the Core Modules section of this documentation.
Keep in mind that the services should not expose how the data is obtained. For instance, if a component asks the service an user, it should not know that this user came from the API. As far as the component know, the user exists and it's all that it need to know.

#### `src/js/Auth/config.js` [9]
This file will read the `.env` file and add the data to the Config Core Module. It can also configure things hard coded way, without the `.env` file. For more information, read the Config Core Module documentation. 

#### `src/js/Auth/core` [10]
In this directory we have the Core Modules of the framework.
> On the future we will remove those modules from here and each one may have it's own package on NPM, like we did with the [SBEE](https://github.com/beeblebrox3/sbee).
See the Core Modules section of this documentation to know more about it.

#### `src/js/shared` [11]
Your project probably have some components or services that are shared by all Apps, like the Not Found page or the Webcam service. You should put those components in here.

#### `src/js/shared/index.js` [12]
You should use this file like the `bootstrap.js` of the Apps.

#### `src/js/shared/layouts` [13]
When you are building an Webapp, some elements are common to all pages, like the menu, footer etc. A lot of pages have the same structure and components placement. What varies is the middle of the page. Is very common that we define a layout that accommodates the common components and have a _slot_ for the content of each page. This directory is for the project layouts.

#### `src/js/shared/pages` [14]
Some pages are global for a webapplication, like the 404 page. You should put this pages in here.

#### `web` [15]
In this directory you must put the html files to load your SPA and all bundles, images, fonts and other assets will be compiled to.


## Core Modules
This package has some modules that make this strutcture work. Let's see them:

### Config
This module is responsible for the application's configurations. The API is very simple (inspired on [Laravel](http://laravel.com/)).

```javascript
App.Config.get("inexistent_config");
App.Config.get("inexistent_config", "default value");
App.Config.set("inexistent_config", "some value");
App.Config.get("inexistent_config");

// shortcut
App.config("existent_config"); // shortcut to App.Config.get
```

The previous code shows how `Config` module works. You have just two methods: `get()` and `set()` .
On the first line, `get()` will return `null`, because there's no config with the name `inexistent_config`.
The second one will return the string "default value", because the second parameter of the `get()` method is the default value if the config doesn't exists.
On the third line we set the "some value" as value to the "inexistent_config" config.
Finally, in the fourth line we have "some value" as returned value, because we set this value for "inexistent_config".
This module is available on all components, libs, services etc, because is defined in the first lines of bootstrap.js file.
The  default configuration values can be set on the `config.js` file on root folder.

Star have a special file to put environment configs. On the root folder, you can see that we have a `.env.example.js`. You can copy this file as `.env.js` to use it and change/add configurations as you need.

You can have multiple env files to represent different environments where the app will run. Star will search for `.env.<environment name>.js` (ie: `.env.dev.js` or `.env.production.js`). You specify the environment on `NODE_ENV` :

```shell
env NODE_ENV=production yarn build // will search for .env.production.js or fallback to .env.js
```

You can override the values from the envfile too:

```shell
env CUSTOM_CONFIG=value yarn build
```

> Tip: You only can override configs that are present on `.env.js`.

On the build process, webpack will create an global variable named `APP_CONFIG`. The `config.js` file will put those configs on the Config instance.

### EventManager
> It was replaced by SBEE, that is a fork the offers the same features plus Buffers for events.

This module, like the name suggests, allows you to fire and observe application's events. Let's see:

```javascript
const eventHandler = function () {
	// do something
};

// eventHandler will be called when an event called "eventName" is fired
App.EventManager.subscribe("eventName", eventHandler);

// eventHandler will be called when "eventA" or "eventB" is fired
App.EventManager.subscribeMultiple(
	["eventA", "eventB"],
	eventHandler
);

// unsubscribe from "eventName"
App.EventManager.unsubscribe("eventName", eventHandler);

// unsubscribe from "eventA" and "eventB"
App.EventManager.unsubscribeMultiple(
	["eventA", "eventB"],
	eventHandler
);

// fire "eventA" and pass to callbacks two parameters
App.EventManager.notify("eventA", "parameter1", "parameter2");
```

You can define as many handlers you want for each event.

**Tip**: the subscrive method returns an function that do the unsubscribe. Sou you can for instance:
```js
const dispose = App.EventManager("EventX", () => console.log("x"));

dispose(); // will unsubscrive
```

> Attention: It's very important to remember to do the unsubscribe thing when your component is unmounted.

### ServicesContainer
This module aims to be a single point to get any service from the application. It allows you to share instances from services between components, for example, without need to pass it over props (react components) or other means, but keeping the flexibility to create new instances if you need it.
Let's see some code:

```javascript
const ServiceXPTO = function () {
};
ServiceXPTO.prototype.sayHi() {
	return "Hi :D";
};

App.ServicesContainer.define("ServiceXPTO", ServiceXPTO);

App.ServicesContainer.get("ServiceXPTO").sayHi(); // returns "Hi :D"
App.ServicesContainer.get("ServiceXPTO").sayHi(); // returns "Hi :D"
App.ServicesContainer.getNewInstance("ServiceXPTO").sayHi(); // returns "Hi :D"
App.ServicesContainer.setInstance("AnotherServiceXPTO", new ServiceXPTO());

// shortcut
App.service("ServiceXPTO").sayHi(); // returns "Hi :D"
```

We use the `define()` (or `register()`) method to register services on ServicesContainer. You can name it like you want. With `get()` method we get an instance of the service. At the first time that this method is called for get a specific service, the container will instantiate this service (` new ServiceName()`) and returns it. Next time you ask for this service, the same instance will be returned. If you need a new instance, even after call `get()` N times before, you can use the method `getNewInstance()`.
The method `setInstance()` allows you to pass to the container the instance that the `get()` method should return. If you don't register the service with the `define()` method, the getNewInstance() will not work. So consider `the setInstance()` as a way to create a [Singleton](https://pt.wikipedia.org/wiki/Singleton).

## Routes and pages
We are using [Page.js](http://visionmedia.github.io/page.js/) for now.

To register your App routes:

```js
// src/js/apps/Auth/routes.js

Router.setDefaultLayout(App.shared.layouts.Application);
Router.addRoute('/', App.Auth.pages.Home);
```

Page.js is very simple. We define a path and what to do when user access it. The framework part is add a easy to use interface to bind paths to components and layouts.

To create dynamic URLs you can use the `:<param>` pattern:
```js
Router.addRoute("/users/:userId", App.components.pages.User);
```
> @todo update when this engine is working

The code above will match with "/users/10", "/users/25" and so on. The value of this parameter will be passed to your component as a prop (`this.props.userId`).

You can create middlewares on the `src/js/routes/middlewares.js` file.

> Tip: the router will pass some props to your page component. The first one is `context`. Is the Page.js context object. Maybe you should avoid it, cause we can remove Page.js in the future. The second one is the all the route named parameters. We also pass all query string parameters (there's a middleware doing that, so you can disable it).

### Router experiments
When rebuilding the Router I made an experiment. Tell me what you think:

```js
Router.addRoute("/users/:userId", App.components.pages.User);
Router.addResolver("userId", (value, context, next) => {
  App.service("Users").find(value).then(user => {
    context.params.user = user;
    next();
  });
});
```

**What is happening:**
We created a route with an parameter named `userId`.
A resolver is a function that can transform and dynamic parameter in another value to pass to the page component. In the example above, we add a resolver to `userId` parameter. Every route that and parameter with this name will call this resolver before the page component (like a middleware by parameter, not route).
In this case, we use the `Users` service to fetch use data (from the backend, for example) and pass it to the page.


> Remember: this is still a work in progress.

